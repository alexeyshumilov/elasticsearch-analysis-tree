import os
import s3publish

def deploy():
    files_to_deploy = []
    for dirname, dirnames, filenames in os.walk('target'):
        for filename in filenames:
            fn = os.path.join(dirname, filename)
            if fn.startswith('target/elasticsearch-analysis-tree-') and fn.endswith('-SNAPSHOT.jar'):
                files_to_deploy.append(fn)
    for fn in files_to_deploy:
        print 'deploying', fn
        print s3publish.put(fn, "s3://shooju/",cache_for=0)