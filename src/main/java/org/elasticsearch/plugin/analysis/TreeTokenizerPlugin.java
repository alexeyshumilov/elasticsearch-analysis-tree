package org.elasticsearch.plugin.analysis;

import org.elasticsearch.index.analysis.AnalysisModule;
import org.elasticsearch.index.analysis.TreeTokenizerFactory;
import org.elasticsearch.plugins.Plugin;

public class TreeTokenizerPlugin extends Plugin {

  @Override
  public String name() {
    return "analysis-tree";
  }

  @Override
  public String description() {
    return "A simple tree analyzer.";
  }

  public void onModule(AnalysisModule module) {
    module.addTokenizer("tree", TreeTokenizerFactory.class);
  }
}
