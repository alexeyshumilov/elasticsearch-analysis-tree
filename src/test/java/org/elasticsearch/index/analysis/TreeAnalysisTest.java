package org.elasticsearch.index.analysis;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.TestHelper;
import org.elasticsearch.common.inject.Injector;
import org.elasticsearch.common.inject.ModulesBuilder;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.settings.SettingsModule;
import org.elasticsearch.env.Environment;
import org.elasticsearch.env.EnvironmentModule;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.IndexNameModule;
import org.elasticsearch.index.settings.IndexSettingsModule;
import org.elasticsearch.indices.analysis.IndicesAnalysisService;
import org.elasticsearch.plugin.analysis.TreeTokenizerPlugin;
import org.junit.Test;

public class TreeAnalysisTest {

  @Test
  public void treeAnalysis() throws IOException {
    String source = "hey\\there\\man";
    String[] expected = { "1\\hey","2\\hey\\there","3\\hey\\there\\man" };
    Analyzer analyzer = initAnalysisService().analyzer("my_tree_tokenizer").analyzer();
    TestHelper.assertTokens(analyzer.tokenStream("_tmp", new StringReader(source)), expected);
  }

  public AnalysisService initAnalysisService() {
    String json = "org/elasticsearch/index/analysis/tree_analysis.json";
    Settings settings = Settings.settingsBuilder()
                                         .loadFromStream(json, getClass().getResourceAsStream(json))
                                         .build();

    Index index = new Index("test_index");

    Injector parentInjector = new ModulesBuilder().add(new SettingsModule(settings),
                                                       new EnvironmentModule(new Environment(settings))).createInjector();

    AnalysisModule analysisModule = new AnalysisModule(settings,
                                                       parentInjector.getInstance(IndicesAnalysisService.class));
    new TreeTokenizerPlugin().onModule(analysisModule);

    Injector injector = new ModulesBuilder().add(new IndexSettingsModule(index, settings),
                                                 new IndexNameModule(index),
                                                 analysisModule).createChildInjector(parentInjector);

    return injector.getInstance(AnalysisService.class);
  }
}
